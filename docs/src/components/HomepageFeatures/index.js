import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Ready to use',
    description: (
      <>
        Basin takes care of configuring the local development environment
        based on one of the available recipes such as Drupal, Nextcloud or wordpress.
        Share the configuration with other developers of the same project by commiting
        it into the repository.
      </>
    ),
  },
  {
    title: 'Few dependencies',
    description: (
      <>
        Basin is distributed as a docker image so you only need docker to run it.
        The tools that it relies on are included in the container such as php, docker-compose or ansible.
      </>
    ),
  },
  {
    title: 'Powered by Docker',
    description: (
      <>
        When the production environments use containers is convenient
        to run those same containers in your local environment using
        the same tools and services in the same version.
      </>
    ),
  },
];

function Feature({title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
