---
sidebar_position: 4
---

# Configuration

## Global and local configuration

A global `config.yml` can exist usually on `$HOME/.config/basin/config.yml`
that will set defaults for all projects.
There is also a local override on per-project located at
`ROOT_OF_THE_PROJECT/.basin/config.yml`.

Have in mind that some settings won't make much sense to configure for each
project like the port the proxy is listening as that is a global setting.

## Set configuration values using the CLI

TODO

## Nomenclature

A configuration setting is referenced using dots meaning the
hierarchy in the YAML structure so for example:
`proxy.https_port`
is saved in the `config.yml` file as:
```yml
proxy:
  https_port: 443
```

## Available settings

### Proxy section

#### `proxy.https_port`
Default: 443
Port that the proxy container will expose on the host for SSL traffic.

#### `proxy.http_port`
Default: 80
Port that the proxy container will expose on the host for non-SSL traffic.
