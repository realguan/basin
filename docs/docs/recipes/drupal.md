---
sidebar_position: 1
---

# Drupal

This recipe is intented to run a single composer-managed drupal site
that manage its configuration from the repository.

## Enable xdebug

Enable the xdebug mode per-environment.

Edit `.basin/config.yml`
```yml
environment:
  development:
    xdebug:
      mode: debug
```

Xdebug will connect to the host machine in the port 9003 when
a web request that include `XDEBUG_TRIGGER` in the query like
http://example.com.localhost/?XDEBUG_TRIGGER=test

Tipically you will have an IDE that listens on that port.
Note that the paths inside the container start with `/var/www/html`
so a mapping is needed for the host path where the project lives.
