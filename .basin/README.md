# Internal developing

Version is configured to `latest-dev` in the `VERSION` file and the recipe is
configured to `Itself` in the `config.yml` file in order to facilitate
internal developing.
