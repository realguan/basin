<?php

namespace Basin\Recipes\Itself\Commands;

use Docker\Context\Context;
use Docker\Docker;
use Docker\API\Model\BuildInfo;
use Robo\Tasks;

/**
 * Docker commands needed to build and release.
 *
 * @see http://robo.li/
 */
class DockerCommands extends Tasks
{
    /**
     * Build docker image from the source.
     *
     * @command docker:build
     */
    public function build($tag = 'latest', $opts = ['dev' => false])
    {
        $vendor = getenv('PACKAGE_VENDOR') ?: getenv('CI_PROJECT_NAMESPACE');

        $docker = Docker::create();

        $this->taskFilesystemStack()->copy(
            'app/bin/basin',
            'app/src/bin/wrapper'
        )->run();

        // Leaves the directory open as long as the context object exists.
        $context = new Context('app/src');
        $inputStream = $context->toStream();
        $dockerfile = 'Cli.Dockerfile';

        $buildStream = $docker->imageBuild($inputStream, [
            'dockerfile' => $dockerfile,
            't' => $vendor . '/basin:' . $tag,
        ]);
        $buildStream->onFrame([$this, 'onFrame']);
        $buildStream->wait();

        if ($opts['dev']) {
            $context = new Context('app/src');
            $inputStream = $context->toStream();
            $buildStream = $docker->imageBuild($inputStream, [
                'dockerfile' => $dockerfile,
                't' => $vendor . '/basin:' . $tag . '-dev',
                'buildargs' => json_encode(['IMAGE_VARIANT' => 'development'])
            ]);
            $buildStream->onFrame([$this, 'onFrame']);
            $buildStream->wait();
        }
    }

    /**
     * Callback for the build process.
     *
     * @ignored-command
     */
    public function onFrame(BuildInfo $buildInfo)
    {
        if ($stream = $buildInfo->getStream()) {
            $this->io()->write($stream);
        }
        if ($error = $buildInfo->getError()) {
            $this->io()->error($error);
        }
    }
}
