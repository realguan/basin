<?php

namespace Basin\Recipes\Itself\Commands;

use Robo\Tasks;

/**
 * Helper commands to update the dependencies.
 *
 * @see http://robo.li/
 */
class UpdateCommands extends Tasks
{
    protected const LOCKED_VERSIONS_FILE_PATH = 'app/src/locked-versions.yml';

    /**
     * Update composer dependencies.
     *
     * @command update:composer
     */
    public function composer()
    {
        return $this
            ->taskComposerUpdate()
            ->workingDir('app/src')
            ->run();
    }

    /**
     * Add new version definition.
     *
     * @command update:add-locked-version
     */
    public function addLockedVersion(
        $name,
        $source,
        $opts = [
            'lock-version' => 'latest',
            'replace-in-files' => '',
            'release-path' => '',
            'major' => '',
            'suffix' => '',
        ],
    ) {
        $locks = yaml_parse_file(self::LOCKED_VERSIONS_FILE_PATH);
        if ($opts['lock-version'] === 'latest') {
            $lastVersion = $this->getContainer()->get('lastversion');
            $opts['lock-version'] = $lastVersion->getLatestVersion($source, $opts['major'] ?? null);
        }
        $locks[$name] = [
            'version' => $opts['lock-version'],
            'source' => $source,
        ];
        if (!empty($opts['replace-in-files'])) {
            $locks[$name]['replace-in-files'] = explode(',', $opts['replace-in-files']);
        }
        if (!empty($opts['release-path'])) {
            $locks[$name]['release-path'] = $opts['release-path'];
        }
        if (!empty($opts['major'])) {
            $locks[$name]['major'] = $opts['major'];
        }
        if (!empty($opts['suffix'])) {
            $locks[$name]['suffix'] = $opts['suffix'];
        }
        $this->say('Added "' . $name .  '" to the list of dependencies');
        return $this
            ->taskWriteToFile(self::LOCKED_VERSIONS_FILE_PATH)
            ->text(yaml_emit($locks))
            ->run();
    }

    /**
     * Update locked versions.
     *
     * @command update:locked-versions
     */
    public function updateLockedVersions()
    {
        $collection = $this->collectionBuilder();
        $lastVersion = $this->getContainer()->get('lastversion');
        $locks = yaml_parse_file(self::LOCKED_VERSIONS_FILE_PATH);
        $locksUpdated = 0;
        foreach ($locks as $lockName => $lock) {
            if (isset($lock['locked'])) {
                continue;
            }
            $this->say('Checking "' . $lockName . '"');
            $latest = $lastVersion->getLatestVersion($lock['source'], $lock['major'] ?? null);
            if (!$latest || $locks[$lockName]['version'] === $latest) {
                continue;
            }
            $locksUpdated++;
            $this->say('Updating "' . $lockName . '" from "' . $locks[$lockName]['version'] . '" to "' . $latest . '"');
            $locks[$lockName]['version'] = $latest;
            foreach ($locks[$lockName]['replace-in-files'] ?? [] as $filepath) {
                // Search in the file for the name of the variable.
                $search = strtoupper($lockName) . '_LOCK_VERSION';
                // Capture the variable name, the equals sign and the current version (until a space) for replacement.
                $searchRegex = '/' . $search . '=\\S+/m';
                $collection->addTask(
                    $this
                        ->taskReplaceInFile('app/' . $filepath)
                        ->regex($searchRegex)
                        ->to($search . '="' . $locks[$lockName]['version'] . ($locks[$lockName]['suffix'] ?? '') . '"')
                );
            }
        }
        if ($locksUpdated === 0) {
            return $this->say('All packages are already updated');
        }
        $collection->addTask(
            $this
                ->taskWriteToFile(self::LOCKED_VERSIONS_FILE_PATH)
                ->text(yaml_emit($locks))
        );
        return $collection->run();
    }
}
