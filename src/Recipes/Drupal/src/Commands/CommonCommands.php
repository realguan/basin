<?php

namespace Basin\Recipes\Drupal\Commands;

use Basin\Commands\RecipeBase;
use Basin\RecipeHelper\SMTP;
use Basin\RecipeHelper\MariaDb;
use Basin\RecipeHelper\Redis;

/**
 * Commands for a Drupal recipe.
 *
 * @see http://robo.li/
 */
class CommonCommands extends RecipeBase
{
    use SMTP;
    use MariaDb;
    use Redis;

    public function drush(array $args)
    {
        $this->runPassThroughCommand('drush', $args);
    }

    public function composer(array $args)
    {
        $this->runPassThroughCommand('composer', $args);
    }

    protected function getDockerCompose()
    {
        $config = $this->getContainer()->get('config');
        $virtualHost = $this->getContainer()->get('virtualhost');
        $composer = $this->getContainer()->get('composer');
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dockerComposeService = $this->getContainer()->get('dockercompose');

        $activeEnvironment = $config->get('environment.active');

        $publicFilesPath = $config->get('docker.volumes.public-files.relative-path');
        $protocol = $config->get('environment.' . $activeEnvironment . '.http.protocol');
        $environmentType = $config->get('environment.' . $activeEnvironment . '.type');
        $phpAssertActive = $config->get('environment.' . $activeEnvironment . '.php.assertActive');

        $updateNotificationsEmails = $config->get('update.mailNotifications');

        // When the project has configuration set already install using those file,
        // but when there is no config, for example when initializating a project,
        // perform a normal site install.
        $OnEmptyDatabaseSiteInstallMode = is_dir('app/config') ?
            '--existing-config' :
            '--site-name="' . $config->get('projectName') . '" standard';

        $dockerCompose = [
            'services' => [
                'web' => [
                    'image' => $config->get('docker.php-image.name') . ':' .
                        $composer->getLockedVersion($config->get('docker.php-image.composer-name')),
                    'environment' => [
                        'VIRTUAL_HOST' => $virtualHost->getAllVirtualHosts(),
                        'LETSENCRYPT_HOST' => $virtualHost->getAllVirtualHosts(),
                        'VIRTUAL_PROTO' => 'fastcgi-drupal',
                        'VIRTUAL_ROOT' => '/var/www/html/web',
                        'HOST_UID' => getenv('HOST_UID'),
                        'HOST_GID' => getenv('HOST_GID'),
                        'ENVIRONMENT_TYPE' => $environmentType,
                        'PHP_ASSERT_ACTIVE' => $phpAssertActive,
                        'DRUPAL_HASH_SALT' => $config->get('drupal.hash_salt') ?? $config->get('cleanProjectName'),
                        'DRUPAL_PUBLIC_PATH' => $publicFilesPath,
                        'DRUPAL_FIELD_UI_PREFIX_EMPTY' => 'TRUE',
                        'DRUPAL_UPDATE_NOTIFICATION_EMAILS' => $updateNotificationsEmails,
                        'DRUPAL_COMMAND_ON_EMPTY_DB' =>
                            'su-exec www-data composer install --no-interaction && ' .
                            'su-exec www-data drush -y site:install ' . $OnEmptyDatabaseSiteInstallMode,
                        'DRUSH_OPTIONS_URI' => $protocol . '://' . $virtualHost->getVirtualHost(),
                        'SWAP_WWWDATA_UIDGID_FOR_HOST' => 'true',
                        'PHP_FPM_ENV_VARS' => [
                            'DRUPAL_HASH_SALT',
                            'DRUPAL_PUBLIC_PATH',
                            'DRUPAL_FIELD_UI_PREFIX_EMPTY',
                            'DRUPAL_UPDATE_NOTIFICATION_EMAILS',
                            'DRUSH_OPTIONS_URI',
                        ],
                    ],
                    'volumes' => [
                        [
                            'type' => 'bind',
                            'source' => getenv('HOST_WORKING_DIR'),
                            'target' => '/var/www/html',
                        ],
                        [
                          'type' => 'volume',
                          'source' => 'public-files',
                          'target' => '/var/www/html/web/' . $publicFilesPath,
                        ],
                        [
                            'type' => 'volume',
                            'source' => 'private',
                            // Default location in wodby images.
                            'target' => '/mnt/files/private',
                        ],
                    ],
                    'labels' => [
                        getenv('LABEL_REVERSE_DNS') . '.subvolume-paths' => $publicFilesPath,
                    ],
                    // Allow for containers on a linux engine to connect to the host.
                    'extra_hosts' => [
                        'host.docker.internal:host-gateway',
                    ],
                ],
                'wait-for-web' => [
                    'image' => 'dokku/wait:' . $lockedVersion->getVersion('wait'),
                    'restart' => 'no',
                    // Give the web container some time to install composer dependencies, drupal, etc.
                    'command' => '-c web:9000 -t 60',
                    'depends_on' => [
                        'web' => ['condition' => 'service_started'],
                    ],
                ],
                'wait' => [
                    'image' => 'alpine:3.17',
                    'restart' => 'no',
                    'command' => 'echo all services started',
                    'depends_on' => [
                        'wait-for-web' => ['condition' => 'service_completed_successfully'],
                    ],
                ],
            ],
            'volumes' => [
                'public-files' => [
                    'external' => false,
                ],
                'private' => [
                    'external' => false,
                ],
            ],
        ];
        if ($config->get('db.type') === 'mariadb') {
            $dockerCompose = $this->addMariadb($dockerCompose, $config);
        }
        if ($config->get('cache.type') === 'redis') {
            $dockerCompose = $this->addRedis($dockerCompose, $config);
        }
        if ($config->get('mail.type') === 'smtp') {
            $dockerCompose = $this->addSMTP($dockerCompose, $config);
        }
        if ($initCommand = $config->get('environment.' . $activeEnvironment . '.init-command')) {
            $dockerCompose = $this->addInitCommand($dockerCompose, $initCommand);
        }
        if ($xDebug = $config->get('environment.' . $activeEnvironment . '.xdebug')) {
            $dockerCompose = $this->addXDebug($dockerCompose, $xDebug);
        }
        return $this->processDockerCompose($dockerCompose, $config);
    }

    protected function addInitCommand(array $dockerCompose, $initCommand)
    {
        $dockerCompose['services']['web']['environment']['DRUPAL_INIT_COMMAND'] = $initCommand;
        return $dockerCompose;
    }

    protected function addXDebug(array $dockerCompose, $xDebug)
    {
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG'] = '1';
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG_REMOTE_HOST'] = 'host.docker.internal';
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG_LOG'] = '/tmp/xdebug.log';
        $dockerCompose['services']['web']['environment']['PHP_XDEBUG_MODE'] = $xDebug['mode'];
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG_REMOTE_HOST';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG_LOG';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'PHP_XDEBUG_MODE';

        return $dockerCompose;
    }
}
