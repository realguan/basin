<?php

namespace Basin\Recipes\NoRecipe\Commands;

use Basin\RecipeHelper\Rand;
use Consolidation\AnnotatedCommand\CommandData;
use Robo\Tasks;

/**
 * Commands to scaffold an app.
 *
 * @see http://robo.li/
 */
class InitCommands extends Tasks
{
    /**
     * Init app files.
     *
     * @param string $recipe Recipe used for the initialization.
     *
     * @return \Robo\ResultData<mixed>
     */
    public function init($recipe, $opts = [
        'skip-install' => false,
        'docker-image-name' => '',
        'docker-image-name-composer' => '',
        'clean-version-string' => false,
        'prefix-version-string' => '',
        'suffix-version-string' => '',
    ])
    {
        $collection = $this->collectionBuilder();

        $recipeDefinition = $this->getContainer()->get('recipes')->getDefinition($recipe);
        $init = $recipeDefinition['init'];

        // Default the composer package to the image name.
        if ($opts['docker-image-name'] && !$opts['docker-image-name-composer']) {
            $opts['docker-image-name-composer'] = $opts['docker-image-name'];
        }

        if (array_key_exists('gitignore', $init)) {
            $this->initGitignore($collection, $init['gitignore']);
        }

        $collection->addTask(
            $this
                ->taskWriteToFile('app/.gitignore')
                ->append(true)
                ->lines([
                    '',
                    '# Ignore sensitive information, cache and history files',
                    '/.basin/secrets.yml',
                    '/.basin/deploy.yml',
                    '/.basin/vendor/',
                    '/.ash_history',
                    '/.bash_history',
                    '/.lesshst',
                    '/.composer/',
                    '/vendor/',
                ])
        );

        $this->initComposer($collection, $init['composer'] ?? [], $opts);

        if (array_key_exists('secrets', $init)) {
            $this->initSecrets($collection, $init['secrets']);
        }

        $collection->addTask(
            $this
                ->taskFilesystemStack()
                ->mkdir('app/.basin')
        );

        $appConfig = ['recipe' => $recipe];

        if ($opts['docker-image-name']) {
            $appConfig['docker']['defaultContainer']['imageName'] = $opts['docker-image-name'];
            $appConfig['docker']['defaultContainer']['imageNameComposer'] = $opts['docker-image-name-composer'];
        }
        if ($opts['clean-version-string']) {
            $appConfig['docker']['defaultContainer']['cleanVersionString'] = true;
        }
        if ($opts['prefix-version-string']) {
            $appConfig['docker']['defaultContainer']['prefixVersionString'] = $opts['prefix-version-string'];
        }
        if ($opts['suffix-version-string']) {
            $appConfig['docker']['defaultContainer']['suffixVersionString'] = $opts['suffix-version-string'];
        }

        $collection->addTask(
            $this
                ->taskWriteToFile('app/.basin/config.yml')
                ->text(yaml_emit($appConfig))
        );

        $result = $collection->run();
        if ($result->wasSuccessful()) {
            $this->say('Project initialized with the recipe: ' . $recipe);
            if (!$opts['skip-install'] && array_key_exists('install', $init)) {
                $this->addIntallSteps($init['install']);
            }
        }
        return $result;
    }

    /**
     * @hook validate init
     */
    public function validateInit(CommandData $commandData)
    {
        $input = $commandData->input();
        $recipe = $input->getArgument('recipe');
        $recipes = $this->getContainer()->get('recipes')->getAll();
        if (!array_key_exists($recipe, $recipes)) {
            throw new \Exception($recipe . ' is not valid. It must be one of: ' . implode(', ', array_keys($recipes)));
        }
        if (file_exists('app/.basin')) {
            throw new \Exception('The application is already initialized because there a .basin folder.');
        }
    }

    protected function initSecrets($collection, $init)
    {
        $secrets = [];
        foreach ($init as $key => $value) {
            foreach ($value as $subkey => $generator) {
                if (!is_string($generator)) {
                    continue;
                }
                $generatorValues = explode(',', $generator);
                $callback = array_shift($generatorValues);
                $secrets[$key][$subkey] = Rand::{$callback}(...$generatorValues);
            }
        }

        $collection->addTask(
            $this
                ->taskWriteToFile('app/.basin/secrets.yml')
                ->text(yaml_emit($secrets))
        );
    }

    protected function initComposer($collection, $init, $opts)
    {
        $config = $this->getContainer()->get('config');
        $init['config']['name'] = 'basin/' . $config->get('cleanProjectName');
        // When there is a create-project step assume the project manages it own composer.
        $composerRoot = 'app';
        if (array_key_exists('create-project', $init)) {
            $collection->addTask(
                $this
                    ->taskComposerCreateProject()
                    ->source($init['create-project'])
                    ->target('/tmp/app')
                    ->noInstall(true)
            );
        } else {
            $composerRoot = 'app/.basin';
            $collection->addTask(
                $this->taskFilesystemStack()
                     ->mkdir('/tmp/app')
            );
            $collection->addTask(
                $this
                    ->taskComposerInit()
                    ->noInteraction()
                    ->projectName($init['config']['name'])
                    ->workingDir('/tmp/app')
            );
        }
        $collection->addTask(
            $this->taskCopyDir(['/tmp/app' => $composerRoot])
        );


        foreach ($init['config'] as $configKey => $configValue) {
            $collection->addTask(
                $this
                    ->taskComposerConfig()
                    ->dir($composerRoot)
                    ->set($configKey, $configValue)
            );
        }
        foreach ($init['config-json'] ?? [] as $configKey => $configValue) {
            $collection->addTask(
                $this
                    ->taskComposerConfig()
                    ->dir($composerRoot)
                    ->option('json')
                    ->set($configKey, json_encode($configValue))
            );
        }

        $requireTask = $this
            ->taskComposerRequire()
            ->dir($composerRoot)
            // TODO: Add ->noInstall(true) when available in robo.
            ->option('no-install')
            // The php installion inside the container executing this don't usually fulfill the requirements.
            ->ignorePlatformRequirements(true);

        // When there is no require section the package to require comes from the CLI option.
        foreach ($init['require'] ?? [$opts['docker-image-name-composer']] as $dependency) {
            $requireTask->dependency($dependency);
        }
        $collection->addTask($requireTask);

        $requireDev = $init['require-dev'] ?? [];
        if ($requireDev) {
            $requireDevTask = $this
                ->taskComposerRequire()
                ->dev()
                ->dir($composerRoot)
                // TODO: Add ->noInstall(true) when available in robo.
                ->option('no-install')
                ->option('with-all-dependencies')
                ->ignorePlatformRequirements(true);
            foreach ($requireDev as $devDependency) {
                $requireDevTask->dependency($devDependency);
            }
            $collection->addTask($requireDevTask);
        }

        $collection->addTask(
            $this
                ->taskComposerInstall()
                ->dir($composerRoot)
                ->ignorePlatformRequirements(true)
        );
    }

    protected function initGitignore($collection, $init)
    {
        if (isset($init['url'])) {
            $collection->addTask(
                $this->taskFilesystemStack()->copy(
                    $init['url'],
                    'app/.gitignore'
                )
            );
        }
        if (isset($init['append'])) {
            $collection->addTask(
                $this->taskWriteToFile('app/.gitignore')
                     ->append(true)
                     ->lines($init['append'])
            );
        }
    }

    protected function addIntallSteps($init)
    {
        $postRun = $this->getContainer()->get('postrun');
        $postRun->addCommand('basin start');
        foreach ($init as $installStep) {
            $installStep = $this->replaceTokens($installStep);
            $postRun->addCommand('basin ' . $installStep);
        }
    }

    protected function replaceTokens($subject)
    {
        $config = $this->getContainer()->get('config');
        $searchAndReplace = [
            '[projectName]' => $config->get('projectName'),
        ];

        return str_replace(array_keys($searchAndReplace), array_values($searchAndReplace), $subject);
    }
}
