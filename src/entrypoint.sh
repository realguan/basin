#!/bin/sh
# Commands that need to run as root before jumping to a non-privileged user.
# Exit as soon as any command fails and do not allow unbound variables.
set -eu

# In order to have permission to use the docker socket the user
# needs to belong to the same docker group as the host.
if [ -n "${HOST_DOCKER_GID}" ]; then
  addgroup -g "$HOST_DOCKER_GID" docker
  addgroup basin docker
fi

# Change the uid and gid of the user to match the host in order to get proper permissions when creating files.
if [ "${HOST_UID}" -ne "$(id -u basin)" ] && [ "${HOST_GID}" -ne "$(id -g basin)" ]; then
  groupmod --gid "$HOST_GID" basin
  usermod --uid "$HOST_UID" --gid "$HOST_GID" basin
fi

# Volumes are created with the root user ownership.
# Change it to the normal user.
chown basin "/home/basin/shared"

# After the root operations exec as the basin user.
su-exec basin Entrypoint.php "$@"
