#!/usr/bin/env php
<?php

/**
 * Wrapper script that simulates the call from the host.
 */
$classLoader = require_once '/home/basin/vendor/autoload.php';

use Docker\API\Model\ContainersCreatePostBody;
use Docker\API\Model\HostConfig;
use Docker\API\Model\Mount;
use Docker\Docker;

$hostWorkingDir = false;
$configPath = '.basin/deploy.yml';
if (
    file_exists($configPath) &&
    ($config = yaml_parse_file($configPath)) &&
    array_key_exists('pathReplacement', $config)
) {
    // The docker daemon needs paths from the machine running it.
    $hostWorkingDir = str_replace(
        $config['pathReplacement']['from'],
        $config['pathReplacement']['to'],
        getcwd()
    );
}

if (!$hostWorkingDir) {
    echo 'Not possible to transform internal path "' . getcwd() . '" into the host path using "' . $configPath . "\"\n";
    exit(1);
}

$docker = Docker::create();

// Similar to the "docker run" from the wrapper script.
$containerConfig = new ContainersCreatePostBody();
$containerConfig->setImage(getenv('PACKAGE_VENDOR') . '/basin:' . getenv('VERSION'));

// Remove binary from call.
array_shift($_SERVER['argv']);

$containerConfig->setCmd($_SERVER['argv']);

$containerConfig->setEnv([
    'VERSION=' . getenv('VERSION'),
    'WRAPPER_SCRIPT_VERSION=' . getenv('WRAPPER_SCRIPT_VERSION'),
    'PACKAGE_VENDOR=' . getenv('PACKAGE_VENDOR'),
    'CODE_HOSTING=' . getenv('CODE_HOSTING'),
    'EXECUTION_ID=' . getenv('EXECUTION_ID'),
    'HOST_GLOBAL_CONFIG_DIR=' . getenv('HOST_GLOBAL_CONFIG_DIR'),
    'HOST_WORKING_DIR=' . $hostWorkingDir ,
    'HOST_HOME_DIR=' . getenv('HOST_HOME_DIR'),
    'HOST_UID=' . getenv('HOST_UID'),
    'HOST_GID=' . getenv('HOST_GID'),
    'HOST_DOCKER_GID=' . getenv('HOST_DOCKER_GID'),
]);


$dockerSocketMount = new Mount();
$dockerSocketMount->setType('bind');
$dockerSocketMount->setSource('/var/run/docker.sock');
$dockerSocketMount->setTarget('/var/run/docker.sock');

$appMount = new Mount();
$appMount->setType('bind');
$appMount->setSource($hostWorkingDir);
$appMount->setTarget('/home/basin/app');

$globalconfigMount = new Mount();
$globalconfigMount->setType('bind');
$globalconfigMount->setSource(getenv('HOST_GLOBAL_CONFIG_DIR'));
$globalconfigMount->setTarget('/home/basin/globalconfig');

$sshMount = new Mount();
$sshMount->setType('bind');
$sshMount->setSource(getenv('HOST_HOME_DIR') . '/.ssh');
$sshMount->setTarget('/home/basin/.ssh');

$sharedMount = new Mount();
$sharedMount->setType('volume');
$sharedMount->setSource('basin-shared');
$sharedMount->setTarget('/home/basin/shared');

$hostConfig = new HostConfig();
$hostConfig->setMounts([
  $dockerSocketMount,
  $appMount,
  $sshMount,
  $sharedMount,
]);

$containerConfig->setHostConfig($hostConfig);

$containerCreateResult = $docker->containerCreate($containerConfig);
$docker->containerStart($containerCreateResult->getId());
$docker->containerWait($containerCreateResult->getId());
$statusCode = $docker
    ->containerInspect($containerCreateResult->getId())
    ->getState()
    ->getExitCode();

fwrite(STDOUT, $docker->containerLogs($containerCreateResult->getId(), ['stdout' => true]));
fwrite(STDERR, $docker->containerLogs($containerCreateResult->getId(), ['stderr' => true]));

$docker->containerDelete($containerCreateResult->getId());

exit($statusCode);
