ARG \
PHP_LOCK_VERSION="8.1.13"
ARG ALPINE_LOCK_VERSION="3.17"
ARG COMPOSER_LOCK_VERSION="2.5.1"
ARG PHPEXTENSIONINSTALLER_LOCK_VERSION="1.5.52"

FROM composer:${COMPOSER_LOCK_VERSION} as composer
FROM mlocati/php-extension-installer:${PHPEXTENSIONINSTALLER_LOCK_VERSION} as php-extension-installer
FROM php:${PHP_LOCK_VERSION}-cli-alpine${ALPINE_LOCK_VERSION}

# Development files will be cleaned at the end for production images.
ARG IMAGE_VARIANT=production

ENV PACKAGE_VENDOR=upstreamable
# A writable dir to place executables.
ENV BASIN_BIN_DIR=/home/basin/vendor/bin
# Variables depending on others need their own ENV statement.
ENV PATH=${BASIN_BIN_DIR}:${PATH} \
  LABEL_REVERSE_DNS="com.${PACKAGE_VENDOR}.basin" \
  GIT_LOCK_VERSION=2.38.2-r0 \
  SUEXEC_LOCK_VERSION=0.2-r2 \
  SHADOW_LOCK_VERSION=4.13-r0 \
  PATCH_LOCK_VERSION=2.7.6-r8 \
  PIP_LOCK_VERSION=22.3.1-r1 \
  WHEEL_LOCK_VERSION=0.38.4-r0 \
  LASTVERSION_LOCK_VERSION="2.4.8" \
  DOCKERCOMPOSE_LOCK_VERSION="2.14.2" \
  ANSIBLE_LOCK_VERSION="7.1.0" \
  ANSISTRANO_LOCK_VERSION="3.14.0" \
  RSYNC_LOCK_VERSION="3.2.7-r0" \
  GO_LOCK_VERSION="1.19.4-r0" \
  GOBIN=/usr/local/bin

WORKDIR /home/basin

COPY --from=php-extension-installer /usr/bin/install-php-extensions $BASIN_BIN_DIR/
COPY --from=composer /usr/bin/composer $BASIN_BIN_DIR/
# Include lint configuration and patches.
COPY .composer .composer
COPY \
  composer.json \
  composer.lock \
  locked-versions.yml \
  phpstan.neon \
  .phpcs.xml \
  ./

# Fail on pipes (like the curl command to download docker-compose)
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# When docker/compose updates moby/buildkit to a recent version in their go.mod file maybe
# go get github.com/docker/compose/v2/cmd/compose@v2.0.1
# Can be attempted.
# Ignore hadolint until https://github.com/hadolint/hadolint/issues/366 is solved.
# hadolint ignore=DL3042
RUN \
  # Create a user named after the package.
  adduser --disabled-password basin && \
  apk add --no-cache \
    # Required by many composer operations. \
    git="$GIT_LOCK_VERSION" \
    # Required by the entrypoint.sh script. \
    su-exec="$SUEXEC_LOCK_VERSION" \
    # Contains usermod and groupmod for entrypoint. \
    shadow="$SHADOW_LOCK_VERSION" \
    # Required by composer-patches. \
    patch="$PATCH_LOCK_VERSION" \
    # Install python packages. \
    py3-pip="$PIP_LOCK_VERSION" \
    # Allow for modern installation of python packages. \
    py3-wheel="$WHEEL_LOCK_VERSION" \
    # Ansible connections. \
    openssh-client-default=9.1_p1-r1 \
    sshpass=1.09-r1 \
    # Compile python packages. \
    alpine-sdk=1.0-r1 \
    py3-cryptography=38.0.3-r0 \
    py3-cffi=1.15.1-r0 \
    py3-bcrypt=4.0.1-r0 \
    py3-pynacl=1.5.0-r1 \
    rsync="$RSYNC_LOCK_VERSION" \
    go="$GO_LOCK_VERSION" \
    && \
  GO111MODULE=auto go get github.com/commander-cli/commander/cmd/commander && \
  # Decode YAML faster.
  install-php-extensions yaml && \
  # Try a pipenv version above 2020.11.15 to install all python dependencies.
  pip install --no-cache-dir \
    lastversion=="$LASTVERSION_LOCK_VERSION" \
    ansible=="$ANSIBLE_LOCK_VERSION" \
    && \
  ansible-galaxy role install --roles-path /usr/share/ansible/roles ansistrano.deploy,"$ANSISTRANO_LOCK_VERSION" && \
  apk del alpine-sdk && \
  if test $IMAGE_VARIANT = "production"; then \
    composer install --no-dev --optimize-autoloader ; \
  else \
    composer install ; \
  fi && \
  composer clear-cache && \
  # Cleanup
  rm /usr/local/bin/phpdbg /usr/local/bin/php-cgi && \
  mkdir \
    # Mount for host global config.
    globalconfig \
    # Create a volume to share files with other containers.
    shared \
    && \
  curl -fSL "https://github.com/docker/compose/releases/download/v${DOCKERCOMPOSE_LOCK_VERSION}/docker-compose-$(uname -s|tr DL dl)-$(uname -m)" --create-dirs -o /usr/local/bin/docker-compose && \
  chmod +x /usr/local/bin/docker-compose

COPY entrypoint.sh Entrypoint.php bin ${BASIN_BIN_DIR}/

RUN \
  if test $IMAGE_VARIANT = "production"; then \
    apk del go && rm -rf tests /home/root/go /usr/local/bin/commander ; \
  fi && \
  chown -R basin:basin /home/basin

# Copy main code at the end so the layers above are not rebuilt when doing common modifications.
# Copy directories.
# They can not be in the same copy instruction as they need the destination directory name.
COPY ansible /etc/ansible
COPY tests tests
COPY cliSrc cliSrc
# New recipes can be addded at runtime.
COPY --chown=basin:basin Recipes Recipes

ENTRYPOINT ["entrypoint.sh"]

LABEL org.opencontainers.image.title=basin \
  org.opencontainers.image.description="Local development environment and deploy tool based on docker" \
  org.opencontainers.image.authors="Rodrigo Aguilera <hi@rodrigoaguilera.net>" \
  org.opencontainers.image.vendor=$PACKAGE_VENDOR \
  org.opencontainers.image.licenses=GPL-3.0-or-later \
  org.opencontainers.image.url=https://basin.upstreamable.com \
  org.opencontainers.image.source=https://gitlab.com/upstreamable/basin/ \
  org.opencontainers.image.documentation=https://basin.upstreamable.com
