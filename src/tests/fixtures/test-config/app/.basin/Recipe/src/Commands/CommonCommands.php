<?php

namespace Basin\Recipes\FakeRecipe\Commands;

use Basin\Commands\RecipeBase;

/**
 * Commands for a Drupal recipe.
 *
 * @see http://robo.li/
 */
class CommonCommands extends RecipeBase
{
    protected function getDockerCompose()
    {
        return [];
    }

    public function existingcommand(array $args)
    {
        $this->say('hello test');
    }
}
