<?php

namespace Basin\tests;

use PHPUnit\Framework\TestCase;

final class ConfigTest extends TestCase
{
    public function testConfigOverridesWithCommands(): void
    {
        chdir('app/src/tests/fixtures/test-config/');
        // Simulate a recipe is chosen.
        $recipe = 'FakeRecipe';

        // Since there is no autoloader, include explicitly.
        include 'app/.basin/Recipe/src/Config.php';
        include 'app/.basin/Recipe/src/Commands/CommonCommands.php';

        $runner = new \Basin\Runner($recipe);
        $statusCode = $runner->execute(['basin' , '-q'], 'Basin', 'latest');

        $config = $runner->getContainer()->get('config');

        $this->assertSame(0, $statusCode);

        $this->assertNull($config->get('non-existing-config'));
        $this->assertSame('configValue1', $config->get('config1'));
        $this->assertSame('localConfigValue', $config->get('localConfig'));
        $this->assertSame('secretValue', $config->get('secretConfig'));
        $this->assertSame('globalOverridden', $config->get('overrideGlobal'));
        $this->assertSame('localOverridden', $config->get('overrideLocal'));

        $this->assertSame('mariadb', $config->get('db.type'));

        $app = $runner->getContainer()->get('application');

        // Check for command existence due to a change in consolidation/annotated-command
        // that didn't register commands from parent classes.
        $this->assertNotNull($app->get('help'));
        $this->assertNotNull($app->get('deploy:release'));
        $this->assertFalse($app->has('non-existing-command'));
        $this->assertTrue($app->has('existingcommand'));
    }
}
