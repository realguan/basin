#!/usr/bin/env php
<?php

/**
 * Default entrypoint for the cli image.
 *
 * See Cli.Dockerfile for more info.
 */

// Use the composer autoloader
$classLoader = require_once 'vendor/autoload.php';

// Show the real command name when showing the symfony command help
// instead of the container internal call.
$_SERVER['PHP_SELF'] = 'basin';

$runner = new \Basin\Runner();
$statusCode = $runner
    ->setClassLoader($classLoader)
    ->execute($_SERVER['argv'], 'Basin', getenv('VERSION'));
exit($statusCode);
