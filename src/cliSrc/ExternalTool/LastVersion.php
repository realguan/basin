<?php

namespace Basin\ExternalTool;

/**
 * Wrapper for the python tool 'lastversion'.
 */
class LastVersion
{
    /**
     * @return string
     */
    public function getLatestVersion(string $source, string $major = null): string
    {
        $opts = ' ';
        if ($major) {
            $opts .= '--major ' . $major . ' ';
        }
        $output = (string) shell_exec('lastversion' . $opts . escapeshellcmd($source));
        return trim($output);
    }
}
