<?php

namespace Basin\RecipeHelper;

trait Redis
{
    protected function addRedis(array $dockerCompose, $config)
    {
        $lockedVersion = $this->getContainer()->get('lockedversion');
        $dockerCompose['services']['redis'] = [
            'image' => $config->get('docker.redis_image') . ':' . $lockedVersion->getVersion('redis'),
            'volumes' => [
                [
                    'type' => 'volume',
                    'source' => 'redis',
                    'target' => '/data',
                ],
            ],
        ];
        $dockerCompose['services']['web']['depends_on']['redis'] = ['condition' => 'service_started'];
        $dockerCompose['services']['web']['environment']['REDIS_HOST'] = 'redis';
        $dockerCompose['services']['web']['environment']['PHP_FPM_ENV_VARS'][] = 'REDIS_HOST';

        $dockerCompose['volumes']['redis'] = [
            'external' => false,
        ];

        return $dockerCompose;
    }
}
