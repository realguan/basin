<?php

namespace Basin\RecipeHelper;

class Rand
{
    public static function rand($min, $max)
    {
        return rand(intval($min), intval($max));
    }

    /**
     * Returns a URL-safe, base64 encoded string of highly randomized bytes.
     *
     * @see \Drupal\Component\Utility\Crypt from Drupal.
     *
     * @param int $count
     *   The number of random bytes to fetch and base64 encode.
     *
     * @return string
     *   A base-64 encoded string, with + replaced with -, / with _ and any =
     *   padding characters removed.
     */
    public static function randomBytesBase64($count = 32)
    {
        return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode(random_bytes($count)));
    }
}
