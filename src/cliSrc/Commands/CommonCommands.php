<?php

namespace Basin\Commands;

use Docker\Docker;
use Robo\ResultData;
use Robo\Tasks;

/**
 * Commands that are always available.
 *
 * @see http://robo.li/
 */
class CommonCommands extends Tasks
{
    /**
     * List all recipes.
     *
     * @return \Robo\ResultData<mixed>
     */
    public function recipesList()
    {
        $this->io()->title('Available recipes:');

        $tableContents = [];
        $recipes = $this->getContainer()->get('recipes')->getAll();
        foreach ($recipes as $recipe) {
            $tableContents[] = [$recipe['name'], $recipe['description']];
        }
        $this->io()->table(
            ['Name', 'Description'],
            $tableContents,
        );
        // Always successful.
        return new ResultData(ResultData::EXITCODE_OK);
    }

    /**
     * Update to the latest version
     *
     * @option $local Update only for this project in the .basin/VERSION file.
     *
     * @command self-update
     *
     * @param string $version
     *   Use a fixed version to update to.
     * @param array<bool> $opts
     *
     * @return \Robo\Result<mixed>
     */
    public function selfUpdate(string $version = 'latest-stable', array $opts = ['local' => false, 'pull' => true])
    {
        if ($version === 'latest-stable') {
            $projectUrl =
                'https://' .
                getenv('CODE_HOSTING') . '/' .
                getenv('PACKAGE_VENDOR') . '/' .
                'basin';
            $lastVersion = $this->getContainer()->get('lastversion');
            $version = $lastVersion->getLatestVersion($projectUrl);
        }
        $filepath = $opts['local'] ? 'app/.basin/VERSION' : 'globalconfig/VERSION';

        if ($opts['pull']) {
            $postRun = $this->getContainer()->get('postrun');
            $postRun->addCommand('docker pull ' . getenv('PACKAGE_VENDOR') . '/basin:' . $version);
        }

        return $this
            ->taskWriteToFile($filepath)
            ->text($version)
            ->run();
    }

    /**
     * Delete all known artifacts created like docker images, volumes, etc.
     *
     * @command global:uninstall
     *
     * @option $delete-config Delete also the global configuration files and secrets.
     */
    public function uninstall($opts = ['delete-config' => false])
    {
        $postRun = $this->getContainer()->get('postrun');
        $postRun->addCommand('echo "Deleting volume"');
        $postRun->addCommand('docker volume rm basin-shared');

        $docker = Docker::create();

        $query = [
            'filters' => json_encode([
                'reference' => [getenv('PACKAGE_VENDOR') . '/basin'],
            ]),
        ];
        $images = $docker->imageList($query);
        foreach ($images as $image) {
            $postRun->addCommand('echo "Deleting image with id: ' .  $image->getId() . '"');
            if ($image->getRepoTags()) {
                $postRun->addCommand('echo "tags: "' . implode(', ', $image->getRepoTags()));
            }
            $postRun->addCommand('docker image rm ' . $image->getId());
        }
        if ($opts['delete-config']) {
            $postRun->addCommand('echo "Deleting directory ' . getenv('HOST_GLOBAL_CONFIG_DIR') . '"');
            $postRun->addCommand('rm -rf "' . getenv('HOST_GLOBAL_CONFIG_DIR') . '"');
        }
    }

    /**
     * Stop all projects and the proxy.
     *
     * @command poweroff
     */
    public function poweroff()
    {
        $docker = $this->getContainer()->get('docker');
        $postRun = $this->getContainer()->get('postrun');
        $label = 'basin.host_working_dir';
        $containersAttached = $docker->getContainersAttachedToProxy();
        foreach ($containersAttached as $container) {
            if ($container->getLabels()->offsetExists($label)) {
                $postRun->addCommand('basin --root ' . $container->getLabels()[$label] . ' stop');
            }
        }
    }
}
