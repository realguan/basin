<?php

namespace Basin\Service;

/**
 * Locked version methods.
 */
class LockedVersion
{
    /**
     * Locked packages keyed by their name;
     * @var array
     */
    protected $lockedVersions;

    /**
     * @return string
     */
    public function getVersion($name): string
    {
        $package = $this->getLocked()[$name];
        if (isset($package['version-prefix'])) {
            $package['version'] = $package['version-prefix'] . $package['version'];
        }
        return $package['version'];
    }

    public function getLocked()
    {
        if ($this->lockedVersions) {
            return $this->lockedVersions;
        }
        $this->lockedVersions = yaml_parse_file('locked-versions.yml');
        return $this->lockedVersions;
    }
}
