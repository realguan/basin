<?php

namespace Basin\Service;

use Consolidation\Config\ConfigInterface;

/**
 * Virtual host methods.
 */
class VirtualHost
{
    public function __construct(public ConfigInterface $config)
    {
    }

    public function getVirtualHost($route = 'main', $environment = null)
    {
        $config = $this->config;

        $activeEnvironment = $environment ?? $config->get('environment.active');
        $routeHost = $config->get('environment.' . $activeEnvironment . '.routes.' . $route);

        if ($routeHost) {
            return $routeHost;
        }

        $projectName = $config->get('projectName');
        $dnsService = $config->get('environment.' . $activeEnvironment . '.dns.service');

        // Development environments typically don't add any additionall suffix.
        // Just ".localhost at" the end from the dns service.
        $suffix = $config->get('environment.' . $activeEnvironment . '.virtualhost.suffix') ??
            $activeEnvironment;

        if (!empty($suffix)) {
            $suffix = '.' . $suffix;
        }

        if (!empty($dnsService)) {
            if ($dnsService === 'localhost') {
                $suffix .= '.localhost';
            } else {
                $suffix .= '.' . $config->get('localAddress') . '.' . $dnsService;
            }
        }
        return $projectName . $suffix;
    }

    /**
     * Get the main route and all domains that need redirection.
     */
    public function getAllVirtualHosts()
    {
        $activeEnvironment = $this->config->get('environment.active');
        $redirects = $this->config->get('environment.' . $activeEnvironment . '.routes.redirects') ?? [];
        $virtualHosts = [$this->getVirtualHost()];
        $virtualHosts = array_merge($virtualHosts, array_keys($redirects));
        return implode(',', $virtualHosts);
    }
}
