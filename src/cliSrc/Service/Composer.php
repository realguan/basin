<?php

namespace Basin\Service;

use Consolidation\Config\ConfigInterface;

/**
 * Composer methods.
 */
class Composer
{
    /**
     * Locked packages keyed by their name;
     * @var array
     */
    protected $lockedPackages;

    public function __construct(
        protected ConfigInterface $config,
    ) {
    }

    /**
     * @return string
     */
    public function getLockedVersion($name): string
    {
        $version = $this->getLockedPackages()[$name]->version;
        if ($this->config->get('docker.defaultContainer.cleanVersionString')) {
            $version = trim($version, 'v');
        }
        if ($this->config->get('docker.defaultContainer.prefixVersionString')) {
            $version = $this->config->get('docker.defaultContainer.prefixVersionString') . $version;
        }
        if ($this->config->get('docker.defaultContainer.suffixVersionString')) {
            $version = $version . $this->config->get('docker.defaultContainer.suffixVersionString');
        }
        return $version;
    }

    public function getLockedPackages()
    {
        if ($this->lockedPackages) {
            return $this->lockedPackages;
        }

        $lockfilePath = 'app/' . $this->config->get('composer.root') . 'composer.lock';
        $packages = json_decode(file_get_contents($lockfilePath))->packages;
        foreach ($packages as $package) {
            $this->lockedPackages[$package->name] = $package;
        }
        return $this->lockedPackages;
    }
}
