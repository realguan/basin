<?php

namespace Basin;

use Robo\Application as RoboApplication;
use Symfony\Component\Console\Input\InputOption;

/**
 * Customize the Robo application.
 */
class Application extends RoboApplication
{
    /**
     * Define new global flags.
     *
     * @param string $name
     * @param string $version
     */
    public function __construct($name, $version)
    {
        parent::__construct($name, $version);

        $this->getDefinition()
            ->addOption(
                new InputOption(
                    '--root',
                    '-r',
                    InputOption::VALUE_NONE,
                    'If specified, use the given directory as working directory.'
                )
            );
    }
}
